## m690-user 11 RKQ1.210607.001 00WW_3_490 release-keys
- Manufacturer: hmd global
- Platform: sdm660
- Codename: SLD_sprout
- Brand: Nokia
- Flavor: m690-user
- Release Version: 11
- Id: RKQ1.210607.001
- Incremental: 00WW_3_490
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: en-GB
- Screen Density: undefined
- Fingerprint: Nokia/Starlord_00WW/SLD_sprout:11/RKQ1.210607.001/00WW_3_490:user/release-keys
- OTA version: 
- Branch: m690-user-11-RKQ1.210607.001-00WW_3_490-release-keys
- Repo: nokia/SLD_sprout
